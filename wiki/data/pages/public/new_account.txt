====== Getting Your Lab Account ======

You will need an ECE IT department account (for some reason, called CAD and Visualization Lab (CVL) account) to use our servers. If you're an intern, ask Devi/Dhruv. If you're a VT student (with a VT PID), go to [[https://computing.ece.vt.edu/wiki/Main_Page|ECE@VT]].