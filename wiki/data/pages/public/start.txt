====== CV-MLP Labs Wiki ======

Welcome to the wiki for the CV-MLP labs at Virginia Tech! This is a shared resource of the [[https://computing.ece.vt.edu/~parikh/ | Devi Parikh's]] [[https://computing.ece.vt.edu/~parikh/CVL.html | Computer Vision Lab]] and [[https://computing.ece.vt.edu/~dbatra/ | Dhruv Batra's]] [[https://mlp.ece.vt.edu/ | Machine Learning and Perception Lab]].

Please use the sidebar to find the information that you're looking for. To be able to access most of the wiki, you will need a [[public:new_account | CVL account]] first.