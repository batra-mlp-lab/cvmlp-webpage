====== Compute Cluster Puppet Server ======

ECE Support runs a puppet server to manage their machines that we piggyback off of.

  * puppeteer.ece.vt.edu (the server)
  * cvmlp (our username on this server, separate from the cvmlp account on LDAP)
  * Bc$mSkS_B9
