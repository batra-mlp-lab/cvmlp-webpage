====== Printer Supply Details ======

We have an OKI C610 (I don't think the n/dn/cdn/dtn suffixes matter) in the lab. The model Number is N31193A.

The official part numbers are (from [[http://www.okidata.com/resources/products/brochure/c610.pdf | here]]):

^ Part Number ^ Description ^
|  44315303   | Cyan Toner Cartridge     |
|  44315302   | Magenta Toner Cartridge  |
|  44315301   | Yellow Toner Cartridge   |
|  44315304   | Black Toner Cartridge    |
|  44315103   | Cyan Image Drum Kit      |
|  44315102   | Magenta Image Drum Kit   | 
|  44315101   | Yellow Image Drum Kit    |
|  44315104   | Black Image Drum Kit     |

^ Amazon Product #  ^ Description             ^
| B00154Z0L4        | Printer Paper           |
| B00DIMDFC2        | Cyan Toner Cartridge    |
| B00DIJJAXS        | Magenta Toner Cartridge |
| B00DIJJJSE        | Yellow Toner Cartridge  |
| B00DIJJ26S        | Black Toner Cartridge   |
| B00M7S62UU        | Cyan Image Drum Kit     |
| B004E2RQSO        | Magenta Image Drum Kit  |
| B005CJ3L46        | Yellow Image Drum Kit   |
| B005DC02HG        | Black Image Drum Kit    |
