======CV-MLP Compute Cluster Admin Documentation======

This document (hopefully…) contains (or points to) all relevant information about maintaining the __CV-MLP Compute Cluster.__ 
  - The list of admins can be found here. 
  - Software installation is described here. 
  - Server building and maintenance notes can be found here. 
  - The old documentation can be found here.

__Current Admins__

  * cvmlp 
  * Dhruv 
  * Devi 
  * Michael
  * Stan
  * Harsh
  * Clint

===== New Software Installation Procedure =====

During the week of 2015/01/05, we re-installed everything and made our machines consistent (modulo hardware configurations such as GPUs). :WARNING: In order to do this, we are now using a program called puppet (run by ECE IT) to manage our machines. Thus, any future software MUST BE INSTALLED VIA PUPPET or else Dhruv will hunt you down and give you very difficult problem sets. :-P 

You have four options for installing software: __yum__, __pip__ (Python-only), __self-downloaded RPMs__, and __source__.

As before, please leave a comment (in the puppet file(s)) that includes your name, the date, what version of the software (if important), and why you added that (if less obvious). More details about the puppet setup can be found in the puppet guide.

==== Installing Software (yum) ====

yum is the package manager used by CentOS (and other Red Hat-based Linux distributions), similar to apt-get on Debian-based distributions (e.g., Ubuntu). For consistency and ease-of-installation across all machines, software will now be taken care of by puppet.

**__Basic Method__**

Install package to test (if it’s the right thing)

''$ (sudo) yum install <package_name> ''

Remove package to ensure puppet installs it (for consistency)

''$ (sudo) yum remove <package_name> ''

Add package to puppet (see puppet basic use for a rough overview)

Log in to cvmlp@puppeteer.ece.vt.edu (Password: Bc$mSkS_B9)

Add the package to ''~/cvmlp/modules/cvmlp_software/manifests/init.pp'' by copying an example from the top of the file and adding it to the end of the cvmlp_software class (look for ''‘# add stuff here comment’'')

E.g., to add a package:
''package { ‘<package_name>’:
    ensure => ‘present’,
}''

''$ (sudo) puppet agent -tv 2>&1 | less -R (though you may want to run it with --noop after -tv to do a dry run first, as seen in basic puppet use)''
	
yum does tab-completion, so it’s often good to type in the first few letters and letting it fill in the rest

To see which packages are available/installed

''$ (sudo) yum list available''

''$ (sudo) yum list installed''


**__Other Software Installation Methods__**

Occasionally, you will not be able to find software (either in general or the required version) via yum. Thus you’ll need a different approach. Here we describe other approaches.

__Methods in Order of Preference__:

  - Existing package (i.e., possible to install via yum or Python’s pip)
  - Package which exists in another repo (i.e., first you install the repo’s RPM (#3), then proceed with installing via yum (#1))
  - RPM from a trusted source
  - Custom RPM created from source install

:NOTE: If you just want to play around with some software and don’t want to fool with puppet, there is often a way to install it locally, such as by changing the Makefile configuration (e.g., ''./configure --prefix=/home/cogswell/usr/''). Anything you install system-wide should be done via puppet (e.g., a self-built/custom RPM which then gets installed puppet).

__pip (Python-based packages)__

See examples in ‘init.pp’ files (which can be found by reading the puppet guide).

__RPMs From Trusted Source__

The recommended way to install RPMs not in a repo via puppet is to host your own repo somewhere then install things through that repo, but we’ve chosen to use a simpler method of storing the RPM on the puppet server.

First, download the RPM to the puppet server in the appropriate ‘files’ directory (see the puppet guide for details). You can do this via wget or download it and then scp it over. Now that RPM can be installed using an example in the ‘init.pp’ files (again, which can be found by reading the puppet guide). You should know that the file path

''~/cvmlp/modules/cvmlpbase_software/files/opencv-gpu-2.4.10.1-1.x86_64.rpm''
can be referenced as 

puppet:///modules/cvmlpbase_software/opencv-gpu-2.4.10.1-1.x86_64.rpm

from within an ‘init.pp’ file (note the lack of the ‘files’ folder).

__RPMs From Source__

Installing from “source” (i.e., the source code provided by the authors) is another way to install new software. Sometimes, this is unavoidable, either due to custom software or packages (or the necessary version) not being in one of the existing yum repos. 

If you find a repo with a package available for the desired software then it may be a good idea to install that repo. Assuming that you can’t find such a package, then it’s fairly simple to create an RPM yourself then install it via puppet. Suppose you’ve run the commands to build your software (as the cvmlp user--not root--on a lab machine), e.g.,

''$ ./configure''
''$ make''

Normally you would then run ''‘$ sudo make install’'', but run the following instead to create a custom RPM that installs the software:

''$ sudo checkinstall --install=no -R --nodoc make install ''

After you enter a short description for the package you’re creating this will run for a while then put an rpm at ''/root/rpmbuild/RPMS/x86_64/''; it will NOT install the software on the machine you run the command from. You might want to first test the RPM to see if it works:

''$ sudo rpm -Uvh /root/rpmbuild/RPMS/x86_64/<new_rpm>.rpm''

If it does seem to work then install it via puppet just like other RPMs are installed.

Some things require tweaking the RPM a little bit (e.g., making it run ldconfig after installing/uninstalling the package). Some examples (e.g., OpenCV) can be found in ''/home/cvmlp/RPMS_README.md'' (i.e., you need to be logged in as the cvmlp user).
puppet Guide

Basic Use covers how to install packages and Advanced Use describes some more about the puppet installation pipeline and how to add a new server.
Basic Use
To access the puppet server:

''$ ssh cvmlp@puppeteer.ece.vt.edu''

''Password: Bc$mSkS_B9''

Within home, there is a folder (it’s actually a link to elsewhere) called ‘cvmlp’.
You want to go to the modules folder:

''$ cd cvmlp/modules''

There are three main modules...
  * cvmlp_software--software that will be generally useful to the lab
  * cloudcv--software needed for the CloudCV project/website
  * webdev--software that is needed for random webdev-related stuff (e.g., WhitteSearch demo, NodeJS for MTurk HITs)
… and two fixed modules
  * cvmlpbase--basic machine configuration (users, groups, file share, etc.)
  * cvmlpbase_software--commonly used software we thought to install during the machine refresh (e.g. Python 2.7, OpenCV 2.4.10.1, Caffe, MATLAB)

Within each of these there are the ‘files’ folder (used for any files you need to copy to a new machine, e.g., an RPM, Bash script) and the ‘manifests’ folder. The ‘manifests’ folder contains the ‘init.pp’ file that you will need to edit. The order of packages/files/etc. in these files does not match the order puppet will run things in. Thus, to ensure one thing happening before another, you need to explicitly state dependencies (as requirements).

After you update a manifest file, see if your changes are parsed correctly by running the following on one of the lab machines:

''$ (sudo) puppet agent -tv --noop''
(more readable output: $ (sudo) puppet agent -tv --noop 2>&1 | less -R)

Next actually update a machine by running:

''$ (sudo) puppet agent -tv''    
(''$ (sudo) puppet agent -tv 2>&1 | less -R'')

We have included some examples of different ways to install things within the ‘init.pp’ files in the 3 main modules. You’re also welcome to browse the other .pp files (e.g., in cvmlpbase_software) for additional examples of how to do things in puppet (or Google around for help).

List of example tasks:

  * install single package from repo
  * install multiple packages from repo
  * install custom rpm
  * run a command
  * install pip package (to system-wide python 2.7)
  * ensure file is present and has certain contents

Advanced Use
In addition to the modules, there are also some cvmlp environment manifest files, site.pp and cvmlp.pp found in: 
''$ cd cvmlp/manifiests''

site.pp contains some very general, ECE department stuff (such as LDAP account) along with the ‘stages’ that we define. Stages are a way to force certain modules to execute before others. We require that ecebase is installed before cvmlpbase, then cvmlpbase_software after it, and so on. Chances are you will not need to touch this.

cvmlp.pp contains all our the labs’ machines (called nodes) and their relevant information. 

For example, godel’s node is show below. The other nodes would not include the cloudcv and webdev classes. In addition, we include one of two cvmlpbase_software sub-classes. 

For godel and other CPU-only machines, we include the nogpu sub-class (i.e., cvmlpbase_software::nogpu). 

For rosenblatt and other GPU machines, we include the gpu sub-class (i.e., cvmlpbase_software::gpu). 

Thus, to add a new machine, you just need to copy-and-paste an existing node and change any necessary information, such and the hostname, the IP, which classes to include, and so on.

''node 'godel.ece.vt.edu' {

  class { 'cvmlpbase': stage => 'cvmlpbase’ }
  class { 'cvmlpbase_software::nogpu': stage => 'cvmlpbase_software' }
  class { 'cvmlp_software': stage => 'cvmlp_software' }
  class { 'cloudcv': stage => 'cloudcv' }
  class { 'webdev': stage => 'webdev' }

  host { 'godel':
	ensure      	=> 'present',
	name        	=> 'godel.ece.vt.edu',
	host_aliases	=> 'godel',
	ip          	=> '128.173.88.252',
  }

  network::if::static { 'eth0':
	ensure	=> 'up',
	ipaddress => '128.173.88.252',
	netmask   => '255.255.252.0',
	gateway   => '128.173.88.1',
	dns1  	=> '128.173.88.31',
	dns2  	=> '198.82.247.98',
	peerdns   => true,
  }
}''
TODO: Add stuff about <vision/ml>.ece.vt.edu links
Server Building/Maintenance
Maintenance
If a (non-SSD) disk ever breaks, it is important that the replaced drive is rebuilt. This can be done via the RAID BIOS menu and possibly via software available for the OS.
Setting Up/Building a New Machine
Hardware
Install CPU chips.
Screw heat-sinks on top. Not sure if arrow points in direction of air flow or not. Last time we chose directions of airflow (away from fan).
Install RAM chips
Add hot-swap drives
CPU1 on 2U chassis tends to be hotter than others. Put a folded piece of paper to direct airflow.
Put heat sink on one chip just outside the added paper partition.
If the server came with a big plastic piece over most of the CPU/RAM area, put it back before closing it up.

OS and Software
Go to LSI SAS bios (second or third screen during bootup) and press Ctrl-C. Create a RAID1 partition with the 2 4TB drives (or 2 RAID partitions on godel for CloudCV). (Manual says this should be done with another utility; that utility could never find any drives.)
Go to BIOS. Change boot sequence so that USB is first and Raid HDD is second. You may have to change the hard disk order so that the RAID HDD is the first device.
NOTE: While building turing and marr, the utility that the manual suggests (Adaptec Raid Utility) could actually see all 3 drives and we create a RAID1 with that.
Boot from Live-drive on USB, but don’t let it auto-boot.
Instead, run Memtest for at least 2 passes. This will take a long long time (1-2 days).
Once that’s done and the machine restarts, you can start installing the OS.
Type in the machine/host name (e.g., godel, turing) (note, this line might be out-of-place)
Choose basic storage option
root password: badR00Tpass (note: zero, not the letter O)
Choose “create custom layout” and have the following partition structure with ext4 (takes about 30 minutes to finish):
SSD
First 20GB (20000 MB) on swap
Remainder on /ssd_local
HDDs (RAID)
First 300GB (300000 MB) on /
Remainder on /hdd_local
Boot Manager:
Set first BIOS device to main RAID drive
Set second BIOS device to SSD
Put bootloader on MBR; should be on first BIOS device (i.e., RAID drive)
Initial user (will get deleted right away)
name: tmp_user
passwd: password
Check box to “Synchronize date and time over the network”
Use default Kdump settings
Reboot
Use virtual terminal (Ctrl+Alt+F2) to log in as root
$ yum update (2 min to DL, then yes to import GPG key, then 10 min to install)
$ chkconfig NetworkManager off
$ service NetworkManager stop
$ userdel tmp_user
Add to /etc/hosts:
128.173.88.76   puppeteer.ece.vt.edu puppet
$ rpm -ivh http://yum.puppetlabs.com/puppetlabs-release-el-6.noarch.rpm
$ yum install puppet (yes to import GPG key)
Add the following lines at the end of /etc/puppet/puppet.conf, where <host> is the machine name (e.g., godel):
server = puppeteer.ece.vt.edu
certname = <host>.ece.vt.edu
environment = cvmlp
Test out puppet:
$ puppet agent -tv --noop 2>&1 | less -R
If that seems to be able to run fine (there will be some errors, due to things like dependencies not being installed), actually let it install:
$ puppet agent -tv 2>&1 | less -R (3 min)
Reboot (lets some share mounting and other things in the ecebase module take effect)
Typically, puppet will try to run after reboot, but just in case, try:
$ puppet agent -tv 2>&1 | less -R
VERY IMPORTANT; otherwise users will not be able to launch >2 matlabs with 12 workers each
Set ulimit max user processes to 16384 from 1024 (in /etc/security/limits.d/90­-nproc.conf):
* soft nproc 16384
root soft nproc unlimited
For GPU machines w/ K40s (currently rosenblatt and tesla):
See /home/cvmlp/gpu_config.sh for further instructions
Make sure that the root password gets changed (Dhruv needs to be told to do this)
Things we’d like to puppet (i.e., make Branden setup):
Add higher limit to 90-nproc.conf
pip shouldn’t try to install every time
Add environment variable to indicate gpu or nogpu to PYTHONPATH
Can we have puppet read some manifest file from /srv/share so admins won’t have to login to cvmlp on puppet?
Old Documentation (pre-2015-01-05)
Instructions for Admins:

Before you install something, check this page. 
If you install something, make a note here. 
Please be consistent. If you install something, install the same version on all machines. It’s a little more work, but in the end, better for everyone. 
Don’t step on other admin’s toes. Before you uninstall packages, ask the person who installed it, and send out an email on the cluster listserv. There might be a reason why things are the way they are. 
Install source:
“standard” packages (say perl): yum
example: sudo yum install perl
python packages (say numpy, matlabplotlib): pip
example: sudo pip install ipython
specialized packages (say Google’s protobuf): from source
example
wget https://protobuf.googlecode.com/files/protobuf-2.5.0.tar.bz2
tar -zxvf protobuf-2.5.0.tar.bz2
cd protobuf-2.5.0
sudo make && make install
Notes:
One user owns everything under /srv/share/data/. There’s a weekly cron job which makes this so.
username: cvmlp
password: Bc$mSkS_B9
We have a user called cvmlp with the same password at puppeteer.ece.vt.edu
cvmlp specific puppet config (used for tesla) at /etc/puppet/environments/cvmlp/


List of packages installed on CV-MLP Compute Cluster
Package Name
Version
Installed from?
Installed by:
Date
List of machines
Notes (e.g. why is this being installed?)
ipython
0.1
yum
Dhruv
04/13/14
godel,turing,marr


python
2.7
source?
Harsh
04/13/14
godel,turing,marr


ZeroMQ
 4.0.4


source
Neelima
04/14/14
godel,marr,turing
requirement for  python matlab bridge
Pyzmq
14.1.0
pip install
Neelima
04/14/14
godel, marr,
requirement for python matlab bridge
python-matlab-bridge


GitHub
Neelima
04/14/14
godel, marr, 
not installing on other servers as they are not connected to cloudcv
xorg-x11-server-Xvfb
1.13.0-23.1.sl6
yum
Dhruv
04/19/14
godel,turing,marr
Faruk needed it for Mainak’s autoclock-in script
“it looks like I need a package called 'xvfb' installed, which will tell Firefox that there is a display even though there is none. Firefox does not open if it thinks there is no display.”
splinter
0.6.0
pip
Harsh
04/24/14
godel, turing, marr
needed for automatic login and logout script written by ankit, used by a lot of people.
pyvirtualdisplay
0.1.5
pip
Harsh
04/24/14
godel,turing, marr
needed for automatic login and logout script written by ankit, used by a lot of people.
hdf5, hdf5-devel
1.8.7
yum
Michael
4/25
rosenblatt
godel, turing, and marr already have these installed. Before more repositories were added I had installed this from soure. I removed that install and replaced it with the repo version.
bzip2-devel 
1.0.5-7
yum
Michael
4/28
rosenblatt
required for matplotlib
freetype-devel
2.3.11-14
yum
Michael
4/28
rosenblatt
required for matplotlib
libjpeg-turbo-devel
1.2.1-3
yum
Michael
4/30
rosenblatt
PIL needed this to read jpegs
libpng-devel
2:1.2.49-1
yum
Michael
4/28
rosenblatt
required for
 matplotlib
libopebblas
0.2.8
source:
http://www.openblas.net/
 Abdullah suggested source installation for multi threaded support. Copied all the libblas libraries into /usr/lib64




Neelima
5/4
rosenblatt
required for caffe
sqlite-devel
3.6.20-1.el6
yum
Michael
6/3
rosenblatt, vapnik
IPython notebook needed sqlite3 in python2.7. Since python2.7 was built from source (to prevent conflict with python2.6) and pysqlite didn’t work (https://pypi.python.org/pypi/pysqlite/2.6.3) I rebuilt pyton2.7 after installing these headers (see below). Installed on vapnik for consistency.
python (reinstall)
2.7
source
Michael
6/3
rosenblatt
See /code/Python-2.7.3.
used ‘./configure USE=”sqlite”’ and ‘make altinstall’.
See the sqlite-devel note (above) for why. 
CUDA
5.5
yum
Michael
6/12
vapnik, minsky
The cuda repo adds version 6.0 by default (since it was released in April). Caffe expects version 5.5.
readline-devel
6.0
yum
Michael
7/3
rosenblatt,
vapnik,
minsky
My ipython install was complaining about a missing readline plugin, so I updated these 3 machines to be consistent with the other 3.
gcc-gfortran
4.4.7-4
yum
Michael
7/15
minsky
numpy / scipy requirement
powertop
2.3-3
yum
Michael
7/22
rosenblatt
now that there are 3 GPUs in the machine and we’re running them at full capacity, I want to check on power just to make sure things are fine
Imagemagick
6.8.9-6


source: http://www.imagemagick.org/script/install-source.php
Neelima
7/27
Rosenblatt, minsky
Needed to install this on Rosenblatt to handle CMYK images for the RCNN code. (Rcnn code uses this). Was already installed on godel, turing, marr, vapnik
gdb
7.2-60
yum
Michael
7/26
rosenblatt
fairly standard, useful for developing caffe code
cmake
3.0.0

source(did not use yum as it was installing a very old version,(2.6.somehting)  i needed a version 2.8 or newer
Neelima
uninstalled on 8/2
installed on 7/28
marr
standard tool(?)
cmake
2.8.11.2-1.el6.x86_64 
3.0.2
from yum Now installed from source
Dhruv
08/02/14
10/14/14
godel, turing, marr, minsky, vapnik, rosenblatt, tesla
Neelima needs 2.8 for building BNG; Qi needed >2.6 for Darwin.
daemonize
1.7.5
from source(only option)
Harsh
08/06/14
godel, rosenblatt.
For running process and daemonize. Needed for some init scripts for the scheduler.
lmdb
9.9.14
from source (couldn’t find it in a repo)
Michael
08/09/14
rosenblatt
Caffe started supporting this as an alternative for leveldb because it is faster and allows more than one process to read a db at once. See notes at /code/lmdb_notes.txt
gflags-devel
1.3.7
yum
Michael
08/09/14
rosenblatt
Another new caffe requirement
czmq-devel
1.4.1-1
yum
Michael
08/24/14
rosenblatt
pymatbridge requirement
matlab 2014a
R2014a
copy from other machine
Michael
08/25/14
rosenblatt
I wanted this version of matlab on rosenblatt for consistency and so I didn’t have to change my script. The default executable still points to R2013a. I left it like that because I remember some problems with matlab versions and caffe, so I didn’t want to screw up anyone’s code.
openmpi-devel
0:1.5.4-2
yum
Michael
09/01/14
rosenblatt
I needed this to play with https://github.com/bruckner/deepViz
graphviz-devel
2.26.0-10
yum
Michael
09/01/14
rosenblatt
Also needed for deepViz
gflags-devel
1.3.7
yum
Michael
09/14/14
vapnik
Caffe requirement
lmdb
0.9.13
source
Michael
09/14/14
turing
Same as for rosenblatt, except the repo didn’t have version 0.9.14
openblas
0.2.11
yum
Michael
10/09/14
rosenblatt
consistency
tk-devel, turbojpeg-devel, libpcap-devel, xz-devel
8.5.7-5, 1.2.1, 1.4.0-1,
4.999.9-0.3
yum
Michael
10/09/14
rosenblatt
consistency
libjpeg-turbo-devel, openjpeg-devel
1.2.1-3, 1.3-10
yum
Michael
10/2614
tesla
wasn’t installed and didn’t catch it till now for some reason
tbb (thread building block)-dev
2.2-3.20090809.el6
yum
Dhruv
11/5/14
godel,turing,marr,minsky,vapnik,mccarthy,rosenblatt, testla
needed for boost and RIGOR
boost
1.41.0
yum 
Dhruv
11/5/14
all (as above)
needed for RIGOR
eigen3
3.2.2
from source
Dhruv
11/5/14
minsky
needed for Darwin
wxGTK-devel
2.8.12-1.el6.rf
yum
Dhruv
11/5/14
minsky
need for Darwin
glibc libgcc libstdc++


debuginfo-insall
Michael
12/1/14
rosenblatt
needed symbols for debugging
valgrind
3.8.1
yum
Michael
12/1/14
rosenblatt



List of Admins:
Dhruv
Devi
Harsh
Neelima
Stan
Michael

Not anymore:
Abdullah
Naman Agrawa