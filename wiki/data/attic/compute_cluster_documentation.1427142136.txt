======CV-MLP Compute Cluster Admin Documentation======

This document (hopefully…) contains (or points to) all relevant information about maintaining the __CV-MLP Compute Cluster.__ 
  - The list of admins can be found here. 
  - Software installation is described here. 
  - Server building and maintenance notes can be found here. 
  - The old documentation can be found here.

__Current Admins__

  * cvmlp 
  * Dhruv 
  * Devi 
  * Michael
  * Stan
  * Harsh
  * Clint

__===== New Software Installation Procedure =====__

During the week of 2015/01/05, we re-installed everything and made our machines consistent (modulo hardware configurations such as GPUs). :WARNING: In order to do this, we are now using a program called puppet (run by ECE IT) to manage our machines. Thus, any future software MUST BE INSTALLED VIA PUPPET or else Dhruv will hunt you down and give you very difficult problem sets. :-P 

You have four options for installing software: __yum__, __pip__ (Python-only), __self-downloaded RPMs__, and __source__.

As before, please leave a comment (in the puppet file(s)) that includes your name, the date, what version of the software (if important), and why you added that (if less obvious). More details about the puppet setup can be found in the puppet guide.

==== Installing Software (yum) ====

yum is the package manager used by CentOS (and other Red Hat-based Linux distributions), similar to apt-get on Debian-based distributions (e.g., Ubuntu). For consistency and ease-of-installation across all machines, software will now be taken care of by puppet.

**__Basic Method__**

Install package to test (if it’s the right thing)

''$ (sudo) yum install <package_name> ''

Remove package to ensure puppet installs it (for consistency)

''$ (sudo) yum remove <package_name> ''

Add package to puppet (see puppet basic use for a rough overview)

Log in to cvmlp@puppeteer.ece.vt.edu (Password: Bc$mSkS_B9)

Add the package to ''~/cvmlp/modules/cvmlp_software/manifests/init.pp'' by copying an example from the top of the file and adding it to the end of the cvmlp_software class (look for ''‘# add stuff here comment’'')

E.g., to add a package:
''package { ‘<package_name>’:
    ensure => ‘present’,
}''

''$ (sudo) puppet agent -tv 2>&1 | less -R (though you may want to run it with --noop after -tv to do a dry run first, as seen in basic puppet use)''
	
yum does tab-completion, so it’s often good to type in the first few letters and letting it fill in the rest

To see which packages are available/installed

''$ (sudo) yum list available''

''$ (sudo) yum list installed''


**__Other Software Installation Methods__**

Occasionally, you will not be able to find software (either in general or the required version) via yum. Thus you’ll need a different approach. Here we describe other approaches.

__Methods in Order of Preference__:

  - Existing package (i.e., possible to install via yum or Python’s pip)
  - Package which exists in another repo (i.e., first you install the repo’s RPM (#3), then proceed with installing via yum (#1))
  - RPM from a trusted source
  - Custom RPM created from source install

:NOTE: If you just want to play around with some software and don’t want to fool with puppet, there is often a way to install it locally, such as by changing the Makefile configuration (e.g., ''./configure --prefix=/home/cogswell/usr/''). Anything you install system-wide should be done via puppet (e.g., a self-built/custom RPM which then gets installed puppet).

__pip (Python-based packages)__

See examples in ‘init.pp’ files (which can be found by reading the puppet guide).

__RPMs From Trusted Source__

The recommended way to install RPMs not in a repo via puppet is to host your own repo somewhere then install things through that repo, but we’ve chosen to use a simpler method of storing the RPM on the puppet server.

First, download the RPM to the puppet server in the appropriate ‘files’ directory (see the puppet guide for details). You can do this via wget or download it and then scp it over. Now that RPM can be installed using an example in the ‘init.pp’ files (again, which can be found by reading the puppet guide). You should know that the file path

''~/cvmlp/modules/cvmlpbase_software/files/opencv-gpu-2.4.10.1-1.x86_64.rpm '' can be referenced as ''<nowiki>‘puppet:///modules/cvmlpbase_software/opencv-gpu-2.4.10.1-1.x86_64.rpm’</nowiki>'' from within an ‘init.pp’ file (note the lack of the ‘files’ folder).

__RPMs From Source__

Installing from “source” (i.e., the source code provided by the authors) is another way to install new software. Sometimes, this is unavoidable, either due to custom software or packages (or the necessary version) not being in one of the existing yum repos. 

If you find a repo with a package available for the desired software then it may be a good idea to install that repo. Assuming that you can’t find such a package, then it’s fairly simple to create an RPM yourself then install it via puppet. Suppose you’ve run the commands to build your software (as the cvmlp user--not root--on a lab machine), e.g.,

''$ ./configure''\\
''$ make''

Normally you would then run ''‘$ sudo make install’'', but run the following instead to create a custom RPM that installs the software:

''$ sudo checkinstall --install=no -R --nodoc make install ''

After you enter a short description for the package you’re creating this will run for a while then put an rpm at ''/root/rpmbuild/RPMS/x86_64/''; it will NOT install the software on the machine you run the command from. You might want to first test the RPM to see if it works:

''$ sudo rpm -Uvh /root/rpmbuild/RPMS/x86_64/<new_rpm>.rpm''

If it does seem to work then install it via puppet just like other RPMs are installed.

Some things require tweaking the RPM a little bit (e.g., making it run ldconfig after installing/uninstalling the package). Some examples (e.g., OpenCV) can be found in ''/home/cvmlp/RPMS_README.md'' (i.e., you need to be logged in as the cvmlp user).
puppet Guide

Basic Use covers how to install packages and Advanced Use describes some more about the puppet installation pipeline and how to add a new server.
Basic Use
To access the puppet server:

''$ ssh cvmlp@puppeteer.ece.vt.edu''

''Password: Bc$mSkS_B9''

Within home, there is a folder (it’s actually a link to elsewhere) called ‘cvmlp’.
You want to go to the modules folder:

''$ cd cvmlp/modules''

There are three main modules...
  * cvmlp_software--software that will be generally useful to the lab
  * cloudcv--software needed for the CloudCV project/website
  * webdev--software that is needed for random webdev-related stuff (e.g., WhitteSearch demo, NodeJS for MTurk HITs)
… and two fixed modules
  * cvmlpbase--basic machine configuration (users, groups, file share, etc.)
  * cvmlpbase_software--commonly used software we thought to install during the machine refresh (e.g. Python 2.7, OpenCV 2.4.10.1, Caffe, MATLAB)

Within each of these there are the ‘files’ folder (used for any files you need to copy to a new machine, e.g., an RPM, Bash script) and the ‘manifests’ folder. The ‘manifests’ folder contains the ‘init.pp’ file that you will need to edit. The order of packages/files/etc. in these files does not match the order puppet will run things in. Thus, to ensure one thing happening before another, you need to explicitly state dependencies (as requirements).

After you update a manifest file, see if your changes are parsed correctly by running the following on one of the lab machines:

''$ (sudo) puppet agent -tv --noop''
(more readable output: $ (sudo) puppet agent -tv --noop 2>&1 | less -R)

Next actually update a machine by running:

''$ (sudo) puppet agent -tv''    
(''$ (sudo) puppet agent -tv 2>&1 | less -R'')

We have included some examples of different ways to install things within the ‘init.pp’ files in the 3 main modules. You’re also welcome to browse the other .pp files (e.g., in cvmlpbase_software) for additional examples of how to do things in puppet (or Google around for help).

List of example tasks:

  * install single package from repo
  * install multiple packages from repo
  * install custom rpm
  * run a command
  * install pip package (to system-wide python 2.7)
  * ensure file is present and has certain contents

Advanced Use
In addition to the modules, there are also some cvmlp environment manifest files, site.pp and cvmlp.pp found in: 
''$ cd cvmlp/manifiests''

site.pp contains some very general, ECE department stuff (such as LDAP account) along with the ‘stages’ that we define. Stages are a way to force certain modules to execute before others. We require that ecebase is installed before cvmlpbase, then cvmlpbase_software after it, and so on. Chances are you will not need to touch this.

cvmlp.pp contains all our the labs’ machines (called nodes) and their relevant information. 

For example, godel’s node is show below. The other nodes would not include the cloudcv and webdev classes. In addition, we include one of two cvmlpbase_software sub-classes. 

For godel and other CPU-only machines, we include the nogpu sub-class (i.e., cvmlpbase_software::nogpu). 

For rosenblatt and other GPU machines, we include the gpu sub-class (i.e., cvmlpbase_software::gpu). 

Thus, to add a new machine, you just need to copy-and-paste an existing node and change any necessary information, such and the hostname, the IP, which classes to include, and so on.

<code>node 'godel.ece.vt.edu' {

  class { 'cvmlpbase': stage => 'cvmlpbase’ }
  class { 'cvmlpbase_software::nogpu': stage => 'cvmlpbase_software' }
  class { 'cvmlp_software': stage => 'cvmlp_software' }
  class { 'cloudcv': stage => 'cloudcv' }
  class { 'webdev': stage => 'webdev' }

  host { 'godel':
	ensure      	=> 'present',
	name        	=> 'godel.ece.vt.edu',
	host_aliases	=> 'godel',
	ip          	=> '128.173.88.252',
  }

  network::if::static { 'eth0':
	ensure	=> 'up',
	ipaddress => '128.173.88.252',
	netmask   => '255.255.252.0',
	gateway   => '128.173.88.1',
	dns1  	=> '128.173.88.31',
	dns2  	=> '198.82.247.98',
	peerdns   => true,
  }
}</code>

:TODO: Add stuff about <vision/ml>.ece.vt.edu links

**__Server Building/Maintenance__**

__Maintenance__

If a (non-SSD) disk ever breaks, it is important that the replaced drive is rebuilt. This can be done via the RAID BIOS menu and possibly via software available for the OS.

__Setting Up/Building a New Machine__

__Hardware__

  * Install CPU chips.
  * Screw heat-sinks on top. Not sure if arrow points in direction of air flow or not. Last time we chose directions of airflow (away from fan).
  * Install RAM chips
  * Add hot-swap drives
  * CPU1 on 2U chassis tends to be hotter than others. Put a folded piece of paper to direct airflow.
  * Put heat sink on one chip just outside the added paper partition.
  * If the server came with a big plastic piece over most of the CPU/RAM area, put it back before closing it up.

__OS and Software__

  * Go to LSI SAS bios (second or third screen during bootup) and press Ctrl-C. Create a RAID1 partition with the 2 4TB drives (or 2 RAID partitions on godel for CloudCV). (Manual says this should be done with another utility; that utility could never find any drives.)
  * Go to BIOS. Change boot sequence so that USB is first and Raid HDD is second. You may have to change the hard disk order so that the RAID HDD is the first device.
  * :NOTE: While building turing and marr, the utility that the manual suggests (Adaptec Raid Utility) could actually see all 3 drives and we create a RAID1 with that.
  * Boot from Live-drive on USB, but don’t let it auto-boot.
  * Instead, run Memtest for at least 2 passes. This will take a long long time (1-2 days).
  * Once that’s done and the machine restarts, you can start installing the OS. <
    * Type in the machine/host name (e.g., godel, turing) (note, this line might be out-of-place)
    * Choose basic storage option
    * root password: badR00Tpass (note: zero, not the letter O)
    * Choose “create custom layout” and have the following partition structure with ext4 (takes about 30 minutes to finish): <
      * SSD
        * First 20GB (20000 MB) on swap
        * Remainder on /ssd_local
      * HDDs (RAID)
        * First 300GB (300000 MB) on / 
        * Remainder on /hdd_local 
    * Boot Manager: 
        * Set first BIOS device to main RAID drive
        * Set second BIOS device to SSD
        * Put bootloader on MBR; should be on first BIOS device (i.e., RAID drive)
      * Initial user (will get deleted right away)
        * name: tmp_user
        * passwd: password
      * Check box to “Synchronize date and time over the network”
      * Use default Kdump settings
      * Reboot
      * Use virtual terminal (Ctrl+Alt+F2) to log in as root
      * $ yum update (2 min to DL, then yes to import GPG key, then 10 min to install)
      * $ chkconfig NetworkManager off
      * $ service NetworkManager stop
      * $ userdel tmp_user
      * Add to /etc/hosts:
        * 128.173.88.76   puppeteer.ece.vt.edu puppet
      * $ rpm -ivh  http://yum.puppetlabs.com/puppetlabs-release-el-6.noarch.rpm

      * $ yum install puppet (yes to import GPG key)
      * Add the following lines at the end of /etc/puppet/puppet.conf, where <host> is the machine name (e.g., godel):
        * server = puppeteer.ece.vt.edu
        * certname = <host>.ece.vt.edu
        * environment = cvmlp
      * Test out puppet:
        * $ puppet agent -tv --noop 2>&1 | less -R
      * If that seems to be able to run fine (there will be some errors, due to things like dependencies not being installed), actually let it install:
        * $ puppet agent -tv 2>&1 | less -R (3 min)
        * Reboot (lets some share mounting and other things in the ecebase module take effect)
      * Typically, puppet will try to run after reboot, but just in case, try:
        * $ puppet agent -tv 2>&1 | less -R
      * VERY IMPORTANT; otherwise users will not be able to launch >2 matlabs with 12 workers each
      * Set ulimit max user processes to 16384 from 1024 (in /etc/security/limits.d/90­-nproc.conf):
        * * soft nproc 16384
        * root soft nproc unlimited
      * For GPU machines w/ K40s (currently rosenblatt and tesla):
        * See /home/cvmlp/gpu_config.sh for further instructions
      * **__Make sure that the root password gets changed (Dhruv needs to be told to do this)__**

Things we’d like to puppet (i.e., make Branden setup):
  * Add higher limit to 90-nproc.conf
  * pip shouldn’t try to install every time
  * Add environment variable to indicate gpu or nogpu to PYTHONPATH
  * Can we have puppet read some manifest file from /srv/share so admins won’t have to login to cvmlp on puppet?