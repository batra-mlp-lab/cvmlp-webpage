====== Using AMT (Amazon Mechanical Turk) ======

__Account Info (CVL)__ \\
E-mail: research.tasks@gmail.com \\
Password: Get via Devi \\
access_key and secret_key: Get via Devi \\

You can make simple HITs via their web interface, but it comes with limitations, such as only being able to download the results once in an hour (so if you’re currently running something and want to see new progress, you’ll have to wait). The Command Line Tools don’t have this issue and let you automatically create, delete, accept/reject work, etc.

__boto__ 
boto is a Python API for accessing Amazon services. Don’t know how to use, but supposed to be very helpful.

__Command Line Tools (CLT)__ 
For CLT, in the main folder, there is a mturk.properties file that you should put the appropriate access_key and secret key in. In addition, there are two service_url values: one for launching things on the sandboxed version (e.g., you can’t waste money while you test out interfaces) and the real version. You can just comment out whichever version you want to run it on. (Note: you can also just pass the appropriate parameter to any of their scripts to run it on sandbox, e.g., ''./loadHITs.sh -sandbox [other options]''.)

__HITTypeId__: The (unique) ID that is associated with a particular type of HIT (e.g., collecting sentence descriptions for images).
__HITId__: The (unique) ID that is associated with each (unique) version of a particular type of HIT (e.g.,  collecting sentence descriptions for a specific set of images)


After downloading the CLT archive, extract it to a folder. (I recommend keeping it in a 3rd_party folder for all external software that you’ll be using for different projects.) Then you would want to export the environment variable so you can access their scripts. I.e., in ''~/.bashrc'', you would append:
''export MTURK_CMD_HOME=/home/santol/cvmlp/3rd_party/aws-mturk-clt-1.3.1'' \\
Now you just need to run the commands in their bin/ directory with appropriate inputs (e.g., configuration, HIT info).

__properties File__
To do external HITs via CLTs, you need a properties file that contains all pertinent HIT information, such as HIT title, pay rate, number of workers per HIT, worker qualifications, etc. Also be sure to double-/triple-check this before you run things (especially when not on sandbox). 

__question File__
This file contains the external HIT itself (i.e., the URL). For example, you can have something like:

<ExternalURL>https://computing.ece.vt.edu/~santol/cvmlp/pose_clipart/sites/clipart_collection/AMT_Pose_Clipart_Plain.html?sent1=${sent1}&sent2=${sent2}&sent3=${sent3}</ExternalURL> &amp;

AMT uses the query string to pass in data for that particular HIT. The query string is everything after the question mark (.html?). You list a parameter, e.g., sent1, followed by an equals sign, and then that parameters value, e.g., ${sent1}. Any additional parameters (e.g., sent2) can be set by separating them by an ampersand (&), but we escape it (not sure if you have to…) by entering ‘&amp;’ instead of just ‘&’. For most simpler HITs, you can probably get away with passing most information via the query string. Sometimes you might have more complicated data that won’t fit in the character-limited query string, so you will need to send filenames (e.g., JSON filenames) that the page will load from your server (e.g., computing.ece.vt.edu) that has all of the data.

input File
The input file is a tab-delimited file whose first row contains the names of the parameters (e.g., sent1, sent2, sent3) that will get substituted into the ExternalURL’s string (e.g., ${sent1}, ${sent2}, ${sent3}). Every (tab-delimited) row underneath contains the (probably row-wise unique) values of those parameters for different versions of the HIT. So if you have 10 of these rows and you have 5 workers do each HIT, you will end up with 10*5 data points.
(You might need to be careful about string formating, though I have yet to run into any issues just having regular strings without any quotes or anything.)

Various Scripts
With all of the “plumbing” in place, you can start managing HITs. I found it convenient to just have some simple scripts (e.g., Bash) that will create the right filenames to pass in as arguments to the CLT functions (e.g., loadHITs.sh). You can write one to start up the HITs, one to automatically approve/reject (with feedback) based on the (unique) AssignmentID. Many of these scripts have various arguments that can be useful, so skim over the UserGuide.html that comes with the CLT folder. For example, loadHITs has the -maxhits # flag that limits how many hits it creates (disregarding the total size of your input file). This is useful when you already have the final, complete input file but you’re still debugging your interface, so there’s no point in launching a billion HITs (it’s a waste of time).

Once you loadHITs, it creates a success file that contains all the different HIT IDs (this can be useful if Devi gets a message from a worker and you need to figure out if it’s from one of your HITs (vs. someone else in the lab)).

Those are the basic concepts of running things via CLT. As with anything that you automate, please be very cautious. Mistakes can waste lab money, result in some bad work being approved, result in good workers (who tend to follow requesters they like) being angry at you, etc. 

approveWork.sh - Either by “approve file” (and individual feedback) or by “success file” without feedback. Probably good to use two rounds of approval via individual feedback then group feedback. BE SURE TO REJECT WORK BEFORE DOING IT BY SUCCESSFILE.

After running rejection/approval, be sure to download again so you’re results file has the up-to-date info (e.g., approvals). This can especially be handy if task isn’t complete yet (i.e., looking at non-approved/reject work only).

rsync -avzhe ssh sites/ filebox:/home/santol/public_html/cvmlp/vqa/