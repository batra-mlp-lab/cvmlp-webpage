====== New Students Info Page ======

First of all, welcome to the lab! I put this together to get people acquainted with some of the basics of the lab.

  * The lab is physically located in __Whittemore 415__. 
  * The keypad code is __**39472**__ (also written on the board inside the lab). 
  * The wireless password to CV-MLP-LAB is __**41539472**__. 
  * The labs servers are located in __Whittemore 432__ (key code: __**16534**__).
----------------------------------------------------------------------------------------------------------

__Listservs__

[[https://www.computing.vt.edu/content/quick-start-listserv#subscribing|Subscribing to a listserv]]

Be sure to be on:

  * cvmlp labs (if local person)\\
  * cvmlp compute\\
  * cvmlrg [[https://filebox.ece.vt.edu/~cvmlreadinggroup/|VT CV-MLP Reading Group]]

Get added to Google Calendar for lab. Contact [[santol@vt.edu|Stanislaw Antol]]

__Server Account__

Setting up CVL/ECE account - either ask Devi/Dhruv if intern, if student, go to [[https://computing.ece.vt.edu/wiki/Main_Page|ECE@VT]]

Computer names are:

  * godel
  * turing
  * marr
  * vapnik
  * minsky
  * mccarthy
  * rosen(blatt)
  * tesla
  * hebb

<computer_name>.ece.vt.edu. 

The name list and their shorter aliases can be found within ''/srv/share/lab_helpful_files/config''

__Setting up password-less SSH__

From personal computer:

''scp -r <userid>@godel.ece.vt.edu:/srv/share/lab_helpful_files/ ~/''

Change <userid> to your CVL account username in the ''~/lab_helpful_files/config'' file and move it to ''~/.ssh''

(''mv ~/lab_helpful_files/config ~/.ssh/'')

''ssh-keygen -t rsa''

Enter a bunch

Make sure ~/ on sever has .ssh folder: login, does ‘cd ~/.ssh’ work? if not, type ''mkdir .ssh''

''scp ~/.ssh/id_rsa.pub godel:~/.ssh/''

On server:

<code>cd ~/.ssh/
cat id_rsa.pub >> authorized_keys2
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys2</code>

Now you should be able to type something like:

ssh godel
On your personal computer and it will login without asking for a password.

__Checking Servers’ Load__

On personal computer (or I guess on the server, if you want).\\
''mv ~/lab_helpful_files/check_lab_load.sh ~/''

Now check_lab_load.sh should be in your home directory and running (assuming you’re in ~/)

''./check_lab_load.sh''

should output all of the servers and how busy they are. Note, for non-GPU machines, the load average should not get above 64 (i.e., the number of cores). For rosenblatt, it shouldn’t go above 8, and for hebb/tesla, it shouldn’t go above 32.

__Lab Printer__
<add>

__Guide to Using Servers__

Screen, htop, etc.

__Research Code__

Dhruv had sent out [[http://www.theexclusive.org/2012/08/principles-of-research-code.html|this link]] and [[http://arkitus.com/patterns-for-research-in-machine-learning/|this link]] a long time ago, which I was heavily inspired by.

__**Web Development (Especially Devi’s students using AMT, but D3 visualizations also very useful for ML projects.)**__

If you want, you can start learning some webdev technologies.

I found these Udacity classes to be helpful (note: I just watched lectures, didn't do assignments):

Intro to HTML and CSS
Web Development
Javascript Basics
Intro to jQuery
Intro to AJAX
Object-Oriented Javascript
Data Visualization and D3.js

You might be able to also access Lynda via VT. I have yet to use it, but it seems to have a lot of relevant lectures (with transcripts, for easy browsing for specific concepts) and one of our IT people suggested it.

Some people in the lab also use Bitbucket. Similar to GitHub, but private repos are free and public are pay-for. It seems good to use something more private for research until you're ready to release it (and adding it to GitHub later is super easy).