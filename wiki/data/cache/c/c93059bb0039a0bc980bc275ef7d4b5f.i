a:23:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:13:"Code Snippets";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:30;}i:4;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:19:"Heat Maps in Matlab";i:1;i:2;i:2;i:30;}i:2;i:30;}i:5;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:30;}i:6;a:3:{i:0;s:4:"file";i:1;a:3:{i:0;s:302:"img = imread(...);
% heatmap is a 2D array
heatmap = immerses(heatmap, ...); % resize to be in the same as image
heatmap = mat2im(heatmap, jet(100), [0 0.2]);
% now heatmap is h*w*3
whatWeNeed = (heatmap + single(img)/255)/2; % if img is already between 0-1 then don’t divide 255

imshow(whatWeNeed);";i:1;s:6:"matlab";i:2;s:10:"heat_map.m";}i:2;i:68;}i:7;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:68;}i:8;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:14:":heat_maps.jpg";i:1;s:0:"";i:2;N;i:3;s:3:"300";i:4;N;i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:398;}i:9;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:421;}i:10;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:423;}i:11;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:28:"Professional Tables in LaTeX";i:1;i:2;i:2;i:423;}i:2;i:423;}i:12;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:2;}i:2;i:423;}i:13;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:423;}i:14;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:61:"http://en.wikibooks.org/wiki/LaTeX/Tables#Professional_tables";i:1;N;}i:2;i:465;}i:15;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:530;}i:16;a:3:{i:0;s:4:"file";i:1;a:3:{i:0;s:1334:"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table} \footnotesize
\setlength{\tabcolsep}{5.5pt}
\begin{center}
\begin{tabular}{@{} l  c  c  c  c  c  c  c @{}}
%\hline
\toprule
& \multicolumn{3}{c}{Open-Answer (OA)} & \multicolumn{3}{c}{ Multiple-Choice (MC)*} & Yes/No \\

%\cline{2-8}
\cmidrule[0.75pt](l){2-4}
\cmidrule[0.75pt](lr){5-7}
\cmidrule[0.75pt](lr){8-8}
& \multicolumn{3}{c}{K} & \multicolumn{3}{c}{ K }& K \\
& 100 & 500 & 1000 & 100 & 500 & 1000 & 2 \\
%\hline
\midrule
Question & 26.91 & 26.68 & 26.64 & 29.60 & 30.18 & 30.18 & 60.11\\
Image & 16.29 & 16.29 & 16.29 & 15.78 & 15.76 & 15.78 & 53.79\\
Caption & 16.08 & 16.03 & 16.03 & 16.11 & 15.98 & 15.90 & 53.22\\
Q+I & 27.28 & 27.13 & 26.78 & 29.70 & 30.47 & 30.15 & 60.02\\
Q+C & 30.34 & 29.70 & 29.36 & 32.29 & 32.86 & 32.56 & 59.16\\
Q+I+C & 30.41 & 28.91 & 29.39 & 32.33 & 32.24 & 32.45 & 59.28\\

\bottomrule
%Human & & & & & & & \\
%\hline
\end{tabular}
\caption{Results on different question tasks for various baselines on real images. All results are the percentage of answers in agreement with human subjects. *\hspace{1pt}The set of questions differ between OA and MC, so MC accuracies may not be strictly higher than OA.}
\vspace{-8pt}
\label{tab:acc}
\end{center}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%";i:1;s:5:"latex";i:2;s:10:"tables.txt";}i:2;i:537;}i:17;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:537;}i:18;a:3:{i:0;s:13:"internalmedia";i:1;a:7:{i:0;s:15:":prof_table.jpg";i:1;s:0:"";i:2;N;i:3;s:3:"200";i:4;N;i:5;s:5:"cache";i:6;s:7:"details";}i:2;i:1898;}i:19;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:0:"";}i:2;i:1922;}i:20;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:1922;}i:21;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1922;}i:22;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:1922;}}