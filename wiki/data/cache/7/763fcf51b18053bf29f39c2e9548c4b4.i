a:43:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:50:"How to Convert a Spreadsheet Into a DokuWiki Table";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1;}i:4;a:3:{i:0;s:12:"externallink";i:1;a:2:{i:0;s:85:"http://donaldmerand.com/code/2012/05/08/convert-a-spreadsheet-to-dokuwiki-format.html";i:1;s:14:"Reference Link";}i:2;i:67;}i:5;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:171;}i:6;a:3:{i:0;s:4:"file";i:1;a:3:{i:0;s:1787:"#!/bin/sh
#tsvfmt - TSV Formatter
#Author: Donald L. Merand
#----------
#Takes a TSV file, and outputs a text-only representation of the table,
#+ justified to the max width of each column. Basically, you use it when 
#+ you want to "preview" a TSV file, or print it prettily.
#----------
#Accepts standard input, or piped/redirected files
#----------
 
#set this to whatever you want to show up between columns
field_separator="|"
tmp_filename=tsv_${RANDOM}_tmp.txt
tmp_widths=tsv_${RANDOM}_widths.txt
 
 
#take file input, or standard input, and redirect to a temporary file
#also, convert mac line-endings to UNIX ones
perl -p -e 's/(:?\r\n?|\n)/\n/g' > "$tmp_filename"
 
#now we're going to extract max record widths from the file
#send the contents to awk
cat "$tmp_filename" |
awk '
BEGIN {  
  FS="\t"
  max_nf = 0 
}
{
  for (i=1; i<=NF; i++) {
    #set the max-length to this field width if it is the biggest
    if (max_length[i] < length($i)) { max_length[i] = length($i) }
  }
  if (max_nf < NF) { max_nf = NF }
}
END {
  for (i=1; i<=max_nf; i++) {
    printf("%s\t", max_length[i])
  }
  printf("\n")
}
' > "$tmp_widths" #store widths in a TSV temp file
 
#now start over by sending our temp file to awk. THIS time we have a widths
#+file to read which gives us the maximum width for each column
cat "$tmp_filename" |
awk -v field_sep="$field_separator" '
BEGIN {
  FS="\t"
  #read the max width of each column
  getline w < "'"$tmp_widths"'"
  #split widths into an array
  split(w, widths, "\t")
  #get the max number of fields
  max_nf = 0
  for (i in widths) { max_nf++ }
}
{
  printf("%s", field_sep)
  for (i=1; i<max_nf; i++) {
    printf("%-*s%s", widths[i], $i, field_sep)
  }
  printf("\n")
}
'
#now we're done. remove temp files
rm "$tmp_filename" "$tmp_widths"
";i:1;s:4:"bash";i:2;s:6:"tsvfmt";}i:2;i:178;}i:7;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:1987;}i:8;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:5:"Steps";i:1;i:3;i:2;i:1987;}i:2;i:1987;}i:9;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:3;}i:2;i:1987;}i:10;a:3:{i:0;s:10:"listu_open";i:1;a:0:{}i:2;i:2004;}i:11;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:2004;}i:12;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:2004;}i:13;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:24:" Download the above file";}i:2;i:2008;}i:14;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:2032;}i:15;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:2032;}i:16;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:2032;}i:17;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:2032;}i:18;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:69:" Save your excel file as type: to “Text (Tab delimited) (*.txt)”.";}i:2;i:2036;}i:19;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:2105;}i:20;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:2105;}i:21;a:3:{i:0;s:11:"listu_close";i:1;a:0:{}i:2;i:2105;}i:22;a:3:{i:0;s:10:"listu_open";i:1;a:0:{}i:2;i:2106;}i:23;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:2106;}i:24;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:2106;}i:25;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:2110;}i:26;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:38:"mv [spreadsheet].txt [spreadsheet].tsv";i:1;s:4:"bash";i:2;N;}i:2;i:2116;}i:27;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:2167;}i:28;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:2167;}i:29;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:2167;}i:30;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:2167;}i:31;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:1:" ";}i:2;i:2171;}i:32;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:46:"bash tsvfmt < [spreadsheet].tsv > [output].txt";i:1;s:4:"bash";i:2;N;}i:2;i:2177;}i:33;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:2236;}i:34;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:2236;}i:35;a:3:{i:0;s:13:"listitem_open";i:1;a:1:{i:0;i:1;}i:2;i:2236;}i:36;a:3:{i:0;s:16:"listcontent_open";i:1;a:0:{}i:2;i:2236;}i:37;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:43:" Copy contents of [output].txt to wiki page";}i:2;i:2240;}i:38;a:3:{i:0;s:17:"listcontent_close";i:1;a:0:{}i:2;i:2283;}i:39;a:3:{i:0;s:14:"listitem_close";i:1;a:0:{}i:2;i:2283;}i:40;a:3:{i:0;s:11:"listu_close";i:1;a:0:{}i:2;i:2283;}i:41;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:2283;}i:42;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:2283;}}