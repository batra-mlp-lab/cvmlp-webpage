a:9:{i:0;a:3:{i:0;s:14:"document_start";i:1;a:0:{}i:2;i:0;}i:1;a:3:{i:0;s:6:"header";i:1;a:3:{i:0;s:33:"Old Building Server Documentation";i:1;i:1;i:2;i:1;}i:2;i:1;}i:2;a:3:{i:0;s:12:"section_open";i:1;a:1:{i:0;i:1;}i:2;i:1;}i:3;a:3:{i:0;s:6:"p_open";i:1;a:0:{}i:2;i:1;}i:4;a:3:{i:0;s:5:"cdata";i:1;a:1:{i:0;s:115:"There might be some useful information in the previous installation instructions, so a messy dump is provided here.";}i:2;i:50;}i:5;a:3:{i:0;s:7:"p_close";i:1;a:0:{}i:2;i:165;}i:6;a:3:{i:0;s:4:"code";i:1;a:3:{i:0;s:7430:"
Installation Steps

Hardware
-- Install CPU chips. 
-- Screw heat-sinks on top. Not sure if arrow points in direction of air flow or not. Last time we chose directions of airflow (away from fan). 
-- Install RAM chips
-- Add hot-swap drives
-- CPU1 on 2U chassis tends to be hotter than others. Put a folded piece of paper to direct air flow. 
-- Put heat sink on one chip just outside the added paper partition. 
– If the server came with a big plastic piece over most of the CPU/RAM area, put it back before closing it up.

Software
-- Go to BIOS. Change boot sequence so that USB is first and SDD is second. You may have to change the hard disk order so that the SSD is the first device.
-- go to LSI SAS bios (second or third screen during bootup). Gave to press ctrl-C. Create a Raid1 partition with 2 4TB drives. (Manual says this should be done with another utility; that utility could never find any drives)
-- NOTE: While building turing and marr, the utility that the manual suggests (Adaptec Raid Utility) could actually see all 3 drives and we create a RAID1 with that. 
-- Boot from Live-drive on USB. 
-- Don't let it auto-boot. 
-- Run Memtest [Will take a long long time; 1-2 days]
-- Install OS the SSD. (But add LSI Logical Volume to the partition structure during installation, i.e. /local below)
-- Partition structure: 
    -- /boot ext4 5GB (we now do 10GB; only godel has 5) on SSD
    -- swap 10GB (we now do 20GB; only godel has 10) on SSD
    – /share 600GB
    -- / whatever is left on SSD [Not anymore; from vapnik, we create 500GB /share and rest (~385GB) on / ). This way if share is full, it doesn't crash the OS. 
    -- /local on HDD 2
-- After installation on first bootup
    -- use NTP for time (required for LDAP)
    -- choose default Keberos settings

So must install AFTER first bootup.
-- create a local user <l_user>
    -- name == l_user
    -- passwd == deleteme
-- su root
-- create a group called admin
    $ groupadd admin
-- gpasswd -a <l_user> admin
-- Set IP address

-- copy over sudoers, hosts, files
             Note: Copying over /etc/sudoers makes sure that the “admin” group has root privileges. 
    -- (from another machine)
$ scp /etc/sudoers root@<new_server>.ece.vt.edu:/etc/sudoers
    -- (from another machine)
$ scp /etc/ssh/sshd_config root@<new_server>.ece.vt.edu:/etc/ssh/sshd_config
(locally)
$ exit  # log out of root
-- start ssh service and ensure it will start at boot
    $ sudo service sshd start
    (Michael: added 8/19/14)
    $ sudo chkconfig sshd on
-- update yum
    $ sudo yum update
-- enable sticky bit for local share, data, and code
    $ sudo chmod 1777 /share
    $ sudo mkdir /share/code
    $ sudo chmod 1777 /share/code
    $ sudo mkdir /share/data
    $ sudo chmod 1777 /share/data

-- (VERY IMPORTANT; otherwise users will not be able to launch >2 matlabs with 12 workers each) 
    -- set ulimit max user processes 16384 (in /etc/security/limits.d/90-nproc.conf )
    max # of open filedescriptors from 1024
#dbatra *          soft    nproc     1024
*          soft    nproc     16384
root       soft    nproc     unlimited

-- Add it's own IP address in /etc/hosts (otherwise ssh'ing into the machine will take a long long time)
    -- sudo vi /etc/hosts
    -- 128.173.88.254    <new_server>.ece.vt.edu <new_server>

-- Add repositories from other machines
    -- exclude sl* repos if any
    (from existing machine)
    $ sudo scp /etc/yum.repos.d/* <new_server>.ece.vt.edu:/etc/yum.repos.d/
    (locally)
    $ sudo rm /etc/yum.repos.d/sl*
    $ sudo scp /etc/pki/rpm-gpg/* <new_server>.ece.vt.edu:/etc/pki/rpm-gpg/
    $ sudo rm /etc/pki/rpm-gpg/RPM-GPG-KEY-sl*
    $ sudo yum update

-- Install MATLAB
    -- cd /usr/local
    -- mkdir MATLAB
             Copy over the newest version of MATLAB available
    -- (from another machine) scp -r R2014a root@<new_server>.ece.vt.edu:/usr/local/MATLAB/
-- Install x2go (for remote desktop)
-- yum install x2goserver

(Michael: added libraries, mostly for caffe, 8/19/14)
$ yum install htop screen
$ yum install gcc gcc-c++ gcc-fortran kernel-devel
-- Install nvidia drivers
    -- Titan Black GPUs require version 334 of nvidia’s driver or later (I used 340.32)
    $ wget http://us.download.nvidia.com/XFree86/Linux-x86_64/340.32/NVIDIA-Linux-x86_64-340.32.run
    (stop X server)
    $ init 3
    -- install driver; choose default options
    $ NVIDIA-Linux
    (start X server)
    $ init 5
$ yum install cuda
$ echo “/usr/local/cuda/lib64” > /etc/ld.so.conf.d/cuda.conf
$ ldconfig
-- these are mostly required for caffe, though some are more generally useful
-- note the use of cmake28; OpenCV complains about earlier versions in CentOS (http://answers.opencv.org/question/24095/building-opencv-247-on-centos-6/)
$ yum install git atlas-devel leveldb-devel snappy-devel boost-devel hdf5-devel openblas-devel gflags-devel cmake28
-- python libraries end up needing these either when you run ./configure for the python2.7 install OR later on when they compile their own code
$ yum install zlib-devel openssl-devel bzip2-devel turbojpeg-devel ncurses-devel readline-devel libpng-devel sqlite-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel
-- Install protobuf (the version in the repo is too old)
    $ wget http://protobuf.googlecode.com/files/protobuf-2.5.0.tar.gz
    $ tar xzvf protobuf-2.5.0.tar.gz
    $ cd protobuf-2.5.0
    $ ./configure
    $ make
    $ make install
-- Install Google’s logging library
$ wget https://google-glog.googlecode.com/files/glog-0.3.3.tar.gz
$ tar zxvf glog-0.3.3.tar.gz
$ cd glog-0.3.3.tar.gz
$ ./configure
$ make
$ make install
$ echo "/usr/local/lib/" > /etc/ld.so.conf.d/local.conf
$ ldconfig
-- Install lmdb
-- installed as suggested by caffe installation notes
-- (http://caffe.berkeleyvision.org/installation.html)
$ git clone git://gitorious.org/mdb/mdb.git
$ cd mdb/libraries/liblmdb
$ make
$ mkdir /usr/local/man
$ make install
-- Install OpenCV 2.4.9
    $ download opencv-2.4.9.zip
$ unzip opencv-2.4.9.zip
$ cd opencv-2.4.9
$ cmake28 ./
$ make
$ make install
-- Install Python 2.7 (WARNING: DO NOT use make install)
    -- CentOS (and yum, which uses python) assumes the default interpreter is version 2.6, so it shouldn't be overwritten. Instead, Python 2.7 is installed next to 2.6 (see instructions) and should be used inside a virtualenv.
$ wget https://www.python.org/ftp/python/2.7.6/Python-2.7.6.tar.xz
$ scp vapnik:/share/code/Python-2.7.6.tar.xz ./
$ tar xJf Python-2.7.6.tar.xz
$ cd Python-2.7.6
(generates Modules/Setup)
$ ./configure --enable-shared --enable-unicode=ucs4
$ make
$ make altinstall
-- NOTE: If you do 'make install' then it can be undone using checkinstall:
--http://stackoverflow.com/questions/16871795/removing-second-python-install
--http://www.patrickmin.com/linux/tip.php?name=checkinstall_fedora_13
-- pip should NOT be installed for python 2.7. Instead, install virtualenv through python 2.6 and use it to create virtual environments with the python 2.7 interpreter. When activated, the pip created for this environment will install (local) packages for python 2.7.
$ yum install python-pip
('$ pip -V' should point to python 2.6)
$ pip install virtualenv
    

Get Branden/ECE IT person to setup puppet/LDAP/etc. Once that’s done:
-- Add LDAP users to local groups, i.e., for all users that should have root access (e.g., Dhruv, Devi)
    -- sudo gpasswd -a <user> admin
";i:1;N;i:2;N;}i:2;i:172;}i:7;a:3:{i:0;s:13:"section_close";i:1;a:0:{}i:2;i:7610;}i:8;a:3:{i:0;s:12:"document_end";i:1;a:0:{}i:2;i:7610;}}