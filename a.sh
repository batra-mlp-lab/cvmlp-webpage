#!/bin/bash

for file in $(find papers/ -name '*.png')
do
  mv $file $(echo "$file" | sed -r 's|.png|.jpg|g')
done
